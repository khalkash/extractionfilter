package com.github.akashworld.Sample;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureWebTestClient
class SampleApplicationTests {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    void postRequestTest() {
        SampleBody request = ImmutableSampleBody.builder()
                .extractionId("extractionComplete")
                .firstProperty("1")
                .secondProperty("2")
                .build();
        SampleBody response = webTestClient.post()
                .uri("/sample")
                .body(BodyInserters.fromValue(request))
                .exchange()
                .expectBody(SampleBody.class)
                .returnResult()
                .getResponseBody();
        assertEquals(request, response);
    }

}
