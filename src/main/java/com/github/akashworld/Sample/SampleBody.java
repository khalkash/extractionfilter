package com.github.akashworld.Sample;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonAutoDetect
@JsonSerialize(as = com.github.akashworld.Sample.ImmutableSampleBody.class)
@JsonDeserialize(as = com.github.akashworld.Sample.ImmutableSampleBody.class)
public abstract class SampleBody {
    @JsonProperty
    public abstract String firstProperty();

    @JsonProperty
    public abstract String secondProperty();

    @JsonProperty
    public abstract String extractionId();
}
