package com.github.akashworld.Sample;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.Exceptions;
import reactor.core.publisher.Mono;

import java.nio.charset.Charset;

@Component
public class ExtractorFilter implements WebFilter {
    private static final Logger LOG = LoggerFactory.getLogger(ExtractorFilter.class);

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        return DataBufferUtils.join(exchange.getRequest().getBody())
                .map(buffer -> buffer.toString(Charset.defaultCharset()))
                .map(this::parseBody)
                .doOnSuccess(sampleBody -> LOG.info(sampleBody.toString()))
                .flatMap(sampleBody -> sampleBody.extractionId().equals("extractionComplete")
                        ? chain.filter(exchange) : produceRejectedResponse(exchange))
                .doOnError(error -> LOG.error(error.toString()))
                .onErrorResume((error) -> produceRejectedResponse(exchange));
    }

    private SampleBody parseBody(String body) {
        try {
            return new ObjectMapper().readValue(body, SampleBody.class);
        } catch (JsonProcessingException e) {
            throw Exceptions.propagate(e);
        }
    }

    private Mono<Void> produceRejectedResponse(ServerWebExchange exchange) {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(HttpStatus.BAD_REQUEST);
        return response.setComplete();
    }
}
