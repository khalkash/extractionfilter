package com.github.akashworld.Sample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class SampleController {
    public static Logger logger = LoggerFactory.getLogger(SampleController.class);

    @PostMapping("/sample")
    public Mono<ResponseEntity> samplePostRequest(@RequestBody SampleBody sampleBody) {
        logger.info(String.valueOf(sampleBody));
        return Mono.just(ResponseEntity.ok(sampleBody));
    }
}
